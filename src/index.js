import Resolver from '@forge/resolver';

const resolver = new Resolver();

resolver.define('getText', (req) => {
  console.log(req);

  return '<h1>Hello, world!</h1>';
});

export const handler = resolver.getDefinitions();
